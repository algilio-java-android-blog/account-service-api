package com.example.accountApi.controller;

import com.example.accountApi.model.Account;

public class AnswerHelper {
    public static String getAmountAnswer(String message, float amount) {
        return message + amount;
    }

    public static String getStatus(Account account) {
        if (account == null) {
            return "Not found";
        } else {
            return account.getStatus();
        }
    }
}
