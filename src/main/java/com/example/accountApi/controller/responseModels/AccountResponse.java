package com.example.accountApi.controller.responseModels;

public class AccountResponse {

    private long accountID;
    private long currency;
    private float amount;
    private String operation;

    public AccountResponse() {

    }

    public AccountResponse(long accountID,long currency, float amount, String operation) {
        this.accountID = accountID;
        this.currency = currency;
        this.amount = amount;
        this.operation = operation;
    }

    public long getAccountID() {
        return accountID;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    public long getCurrency() {
        return currency;
    }

    public void setCurrency(long currency) {
        this.currency = currency;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}

