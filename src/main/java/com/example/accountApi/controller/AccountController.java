package com.example.accountApi.controller;

import com.example.accountApi.controller.responseModels.AccountResponse;
import com.example.accountApi.controller.responseModels.StatusResponse;
import com.example.accountApi.exception.BadRequestException;
import com.example.accountApi.model.Account;
import com.example.accountApi.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;

@RestController
@RequestMapping("/financial")
public class AccountController {

    @Autowired
    private AccountServiceImpl accountServiceImpl;

    @GetMapping(path = "/status/{accountID}")
    @ResponseBody
    public ResponseEntity<StatusResponse> getStatus(@PathVariable long accountID) throws AccountNotFoundException {
        StatusResponse response = new StatusResponse(accountServiceImpl.getAccountById(accountID).getStatus());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping(path = "/inquiry/{accountID}")
    @ResponseBody
    public ResponseEntity<Account> getInquiry(@PathVariable long accountID) throws AccountNotFoundException {
        return new ResponseEntity<>(accountServiceImpl.getAccountById(accountID), HttpStatus.OK);
    }

    @PostMapping(path = "/debit/{accountID}/{currency}/{amount}")
    @ResponseBody
    public ResponseEntity<AccountResponse> debitAccount(@PathVariable long accountID,
                                                        @PathVariable long currency,
                                                        @PathVariable float amount)
            throws BadRequestException {
        Account account = accountServiceImpl.performOperation(accountID, currency, amount, true);
        return new ResponseEntity<>(new AccountResponse(account.getAccountID(), account.getCurrency(), amount, "debit"), HttpStatus.OK);
    }

    @PostMapping(path = "/credit/{accountID}/{currency}/{amount}")
    @ResponseBody
    public ResponseEntity<AccountResponse> creditAccount(@PathVariable long accountID,
                                                         @PathVariable long currency,
                                                         @PathVariable float amount)
            throws BadRequestException {
        Account account = accountServiceImpl.performOperation(accountID, currency, amount, false);
        return new ResponseEntity<>(new AccountResponse(account.getAccountID(), account.getCurrency(), amount, "credit"), HttpStatus.OK);
    }
}
