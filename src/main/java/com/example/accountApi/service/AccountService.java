package com.example.accountApi.service;

import com.example.accountApi.exception.AccountNotFoundException;
import com.example.accountApi.model.Account;

public interface  AccountService {
    Account getAccountById(long accountID) throws AccountNotFoundException;
    Account performOperation(long accountID, long curency, float amount, Boolean debit);
}
