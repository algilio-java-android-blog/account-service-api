package com.example.accountApi.service;

import com.example.accountApi.exception.AccountNotFoundException;
import com.example.accountApi.exception.BadRequestException;
import com.example.accountApi.model.Account;
import com.example.accountApi.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account getAccountById(long accountID) {
        Account account;
        if (accountRepository.findById(accountID).isEmpty()) {
            throw new AccountNotFoundException();
        } else {
            account = accountRepository.findById(accountID).get();
        }
        return account;
    }

    public Account performOperation(long accountID, long currency, float amount, Boolean debit) {
        Account account = getAccountById(accountID);
        Boolean ok = account.performOperation(currency, amount, debit);
        if (!ok) {
            throw new BadRequestException();
        } else {
            accountRepository.save(account);
        }
        return account;
    }
}
