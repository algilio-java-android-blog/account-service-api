package com.example.accountApi.model;

import javax.persistence.*;

@Entity
@Table(name = "account")
public class Account {

    private long accountID;
    private long currency;
    private float balance;
    private String status;

    public Account() {

    }

    public Account(long currency, float balance, String status) {
        this.currency = currency;
        this.balance = balance;
        this.status = status;
    }

    @Id
    public long getAccountID() {
        return accountID;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    @Column(name = "currency", nullable = false)
    public long getCurrency() {
        return currency;
    }

    public void setCurrency(long currency) {
        this.currency = currency;
    }

    @Column(name = "balance", nullable = false)
    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getCreditAmount(float amount) {
        return amount * -1;
    }

    public Boolean validCurrency(long currency) {
        if (getCurrency() != currency) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean validAmount(float amount) {
        if (amount <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean validStatus() {
        if (!getStatus().equalsIgnoreCase("ACTIVE")) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean validBalance(float ammount) {
        if (getBalance() + ammount < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean performOperation(long currency, float amount, Boolean debit) {
        if (!validCurrency(currency) || !validAmount(amount) || !validStatus()) {
            return false;
        }
        if (debit) {
            setBalance(getBalance() + amount);
        } else {
            if (!validBalance(getCreditAmount(amount))) {
                return false;
            }
            setBalance(getBalance() + getCreditAmount(amount));
        }
        return true;
    }
}

